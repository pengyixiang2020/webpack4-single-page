import $ from 'jquery'
import '@popperjs/core'
import 'bootstrap/dist/js/bootstrap.bundle.min'
import 'bootstrap/scss/bootstrap.scss'
import '../css/home.scss'
import html from 'art/home/index.art'

$('#app').html(html)

