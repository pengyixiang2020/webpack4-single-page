// import 'lib-flexible'
// import VSS from './css/main.scss'
import _ from 'lodash/array'
import $ from 'jquery'
import './css/main.scss'
import photo from './assets/img/photo.jpg'
import { add } from './utils/index.js'
import forTest777 from './utils/test.js'
// import moment from 'moment'
// import '../node_modules/moment/locale/zh-cn'

// moment.locale('zh-cn')

// console.log('时间打印：', moment('20111031', 'YYYYMMDD').fromNow())
// console.log('外部链接测试', window)

console.log('lodash:', _.join(['a', 'b', 'c'], '~'))

const btn = document.createElement('button')
btn.innerHTML = '异步加载模块'

btn.onclick = async () => {
  /**
   * 第一种模块异步加载模式
   */
  // import(/* webpackChunkName: "jquery" */ 'jquery').then($ => {
  //   console.log('加载jquery', $)
  // })

  /**
   * 第二种模块异步加载模式
   */
  // const $ = await import(/* webpackPrefetch: true *//* webpackChunkName: "jquery" */ 'jquery')
  // console.log('加载jquery', $)
}

document.body.appendChild(btn)

document.querySelector('#app').classList.add('test')
const img = document.createElement('img')
img.src = photo

add()

document.body.appendChild(img)

/* eslint-disable */
console.log('自动加载模块：', $('#app'))
/* eslint-enable */

Promise.resolve().then(function () {
  // alert(1235)
  const ppp = forTest777
  console.log('环境打印:', process.env.TEST, ppp)
  // console.log(_)
})
