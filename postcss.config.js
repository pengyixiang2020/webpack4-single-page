/**
 * 本文件的执行顺序是自上而下的！！！
 */
module.exports = {
  plugins: {
    /* 需配合lib-flexible使用，lib-flexible默认采用10等分进行适配 */
    'postcss-sprites': {
      /*
        配置精灵图生成的地址，但是由于图片最后会经过url-loader和file-loader的处理
        所以最后输出的路径时url-loader的outPath,但是如果在name中console.log
        会发现,精灵图生成的路径就是spritePath
      */
      /* 精灵图生产的目录 */
      spritePath: './dist/static/sprite',
      /*
        如果发现页面可以显示图片，但未看到打包的文件，
        请检查url-loader是否有转为Base64的配置
      */
      /* ['../assets/sprite/home.png?_sprite']的图片才被打包成精灵图 */
      filterBy: (image) => {
        const index = image.originalUrl.lastIndexOf('?')
        const isFilter = image.originalUrl.substr(index).includes('?_sprite')

        return (isFilter ? Promise.resolve() : Promise.reject())
      }
      /* 图片分组，未配置 */
      // groupBy: (image) => { }
    },
    'autoprefixer': {},
    // 'postcss-pxtorem': {
    //   rootValue: 37.5,
    //   propList: ['*'],
    // },
    /* https://blog.csdn.net/Charissa2017/article/details/105420971 */
    /*
      postcss-px-to-viewport需置于postcss-sprites的后面，
      这样精灵图才能自适应
    */
    'postcss-px-to-viewport': {
      unitToConvert: 'px', // 默认值，需要转换的单位
      viewportWidth: 750, // 窗口宽度，即750设计稿宽度
      unitPrecision: 5, // 保留的小数位
      propList: ['*'], // 要转换的样式属性。如：['height']只转换高
      viewportUnit: 'vw', // 默认值，最终要转换的单位
      fontViewportUnit: 'vw', // 默认值，font-size最终要转换的单位
      selectorBlackList: [
        '.van-', '.ignore-'
      ], // 不做转换的类前缀
      mediaQuery: true // 默认值，不转换媒体查询中的单位。如果转换font-size也一并会转换
      // exclude: [/css/] // 不做转换的目录
    }
  }

}
