
const path = require('path') // 导入Node.js 核心模块，用于操作文件路径。
const webpack = require('webpack')

module.exports = {
  entry: {
    lodash: ['lodash'],
    jquery: ['jquery']
  }, // 入口
  output: {
    /* #文件名称 */
    filename: '[name].dll.js',
    /* #输出目录 */
    path: path.resolve(__dirname, '../dll'),
    /* #库暴露的变量名 */
    library: '[name]',
    /* #库访问的方式 */
    libraryTarget: 'var'
  }, // 出口
  plugins: [
    new webpack.DllPlugin({
      path: path.join(__dirname, '../dll', '[name]-manifest.json'),
      name: '[name]'
    })
  ]
}
