/**
 * 和模式相关的配置都移到package.json中采用webpack cli进行配置
 */

// const mode = 'production' // 模式

const path = require('path')
const { merge } = require('webpack-merge')
const glob = require('glob-all') // 用于匹配文件
const PurgecssPlugin = require('purgecss-webpack-plugin') // css tree shaking 插件
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const common = require('./webpack.common.js')

module.exports = merge(common, {
  // mode: mode,
  /* #source map */
  // devtool: 'cheap-module-source-map',
  optimization: {
    /* #开启js Tree-shaking */
    usedExports: true,
    /* #代码分割 */
    splitChunks: {
      // https://zhuanlan.zhihu.com/p/110173795
      // 分割方式
      chunks: 'all',
      // 被分割的代码体积不小于30kb才分割，单位byte
      minSize: 30000,
      minChunks: 1,
      maxAsyncRequests: 5,
      maxInitialRequests: 3,
      // 名称分割符
      automaticNameDelimiter: '~',
      name: true,
      cacheGroups: {
        /* #处理node_modules目录下导入的模块 */
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10
        },
        // /* #自定义一个处理utils目录下导入的模块 */
        // utils: {
        //   test: /[\\/]utils[\\/]/,
        //   priority: -11 // 优先级配置项
        // },
        /* #处理任意目录导入的模块 */
        default: {
          // https://zhuanlan.zhihu.com/p/110175375
          // 被引用的次数大于2且不小于minSize时被打包
          // 目前知道的是引用次数与入口数(entry)有关
          minChunks: 1,
          // 优先级配置项,越大越先处理
          // 优先级避免了模块重复打包
          priority: -20,
          // 复用分割的代码
          reuseExistingChunk: true
        }
      }
    }
  },
  plugins: [
    new PurgecssPlugin({
      /* #因为webpack的配置文件被放置到build中,__dirname指向的不是根目录而是build了 */
      /* #另外html注释的代码中包含的class不会被tree-shaking,需要删除注释的代码 */
      paths: glob.sync([
        path.join(__dirname, '../public/**/*.html'),
        path.join(__dirname, '../src/view/*.html'),
        path.join(__dirname, '../src/art/**/*.art'),
        path.join(__dirname, '../src/**/*.js')
      ]),
      /* #https://purgecss.com/whitelisting.html#in-the-css-directly */
      /* #白名单包含:.random, #yep, button 这些形式 */
      // whitelist: ['random'] // 需指明明确的类
      // whitelistPatterns: () => {
      //   return [/^whitelisted-/]
      // } // [.whitelisted-tt]
      whitelistPatternsChildren: () => {
        return [/^whitelisted-/]
      } // 可以包含子选择器[.whitelisted-tt .cc或.whitelisted-tt或#whitelisted-tt]
    }), // 对匹配到的文件进行css tree-shaking
    new BundleAnalyzerPlugin()
  ]
})
