const mode = 'development' // 模式

const { merge } = require('webpack-merge')
const chokidar = require('chokidar') // 监视文件变化
const common = require('./webpack.common.js')

module.exports = merge(common, {
  mode: mode,
  /* #source map */
  devtool: 'cheap-module-eval-source-map',
  devServer: { // 打包好的文件直接写入内存
    contentBase: './dist',
    /* #填写ip地址，让devserver服务器可以被外部访问 */
    // host: '192.168.0.105',
    /* #静态文件压缩 */
    compress: true,
    /* #自动打开浏览器 */
    open: true,
    /* #热更新 */
    hot: true,
    /* #true: 不进行页面刷新，不论是否有热更新 */
    hotOnly: true,
    /* #浏览器信息输入级别，这里是warning及以上 */
    clientLogLevel: 'warn',
    /* #关闭终端界面启动的相关信息 */
    // quiet: true,
    /* #端口 */
    port: 8080,
    /* #不启用https服务 */
    https: false,
    /* #将编译时的错误信息显示在浏览器上 */
    overlay: {
      warnings: true, // 显示提示信息
      errors: true // 显示错误信息
    },
    /* #显示加载进度 */
    progress: true,
    proxy: {
      '/api-dev': {
        target: 'http://127.0.0.1:3000', // 跨域目标地址
        // changeOrigin: true, // 开启域名请求
        pathRewrite: { '^/api-dev': '' } // 重写路径
        // secure: false // false: 接受证书无效的https请求
      }
    },
    before(app, server) {
      chokidar.watch([
        './public/**/*.html',
        './src/**/*.html', // 监听html文件的改变并刷新整个页面
        './src/art/**/*.art',
        './src/**/*.js' // 监听js文件的改变并刷新整个页面
      ]).on('all', function (event, path) {
        server.sockWrite(server.sockets, 'content-changed')
      })
    }
  },
  plugins: []
})
