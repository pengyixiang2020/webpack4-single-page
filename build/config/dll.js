const fs = require('fs')
const path = require('path')
const webpack = require('webpack')
const AddAssetHtmlPlugin = require('add-asset-html-webpack-plugin') // 自动将css或js插入index.html

const dllPath = path.resolve(__dirname, '../../dll') // 动态读取要插入的动态插入的库和要拆分的包
const plugins = []
const addAssetHtmlPluginArray = []

fs.readdirSync(dllPath).forEach((file) => {
  const filePath = `../../dll/${file}`
  const ext = path.extname(file)
  if (ext === '.js') {
    addAssetHtmlPluginArray.push({
      filepath: path.resolve(__dirname, filePath),
      // 以?contenthash的形式添加hash，且文件内容不改变hash值不变
      hash: true,
      // 输出目录
      outputPath: 'static/vendor',
      // 公共目录
      publicPath: 'static/vendor'
    })
  } else if (ext === '.json') {
    plugins.push(
      new webpack.DllReferencePlugin({
        manifest: path.resolve(__dirname, filePath)
      })
    )
  }
})

plugins.push(
  new AddAssetHtmlPlugin(addAssetHtmlPluginArray)
)

module.exports = plugins
