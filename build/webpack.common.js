/**
 * # 特别说明
 * 1.【仅作展示】：表示默认值和配置的一样，只是标示出来
 * 2. 打包上线的时候需要将[hash:8]改为[contenthash:8]以实现缓存
 *   详情：https://juejin.im/post/6844903950156578824
 * 3. 由于webpack的相关配置放置在build文件夹中，__dirname指向的是build文件夹
 */
const outputPath = 'dist/' // 根目录
const assetsDir = 'static/' // 文件输出的目录
const hashType = '[hash:8]' // hash类型 [hash:8] | [contenthash:8]

const path = require('path') // 导入Node.js 核心模块，用于操作文件路径。
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin') // 自动构建index.html等
const { CleanWebpackPlugin } = require('clean-webpack-plugin') // 打包前自动清除旧打包生成目录
const MiniCssExtractPlugin = require('mini-css-extract-plugin') // 导入css单独提取的插件
const HappyPack = require('happypack') // 多线程打包

const plugins = require('./config/dll')

module.exports = {
  entry: {
    app: './src/js/home.js'
    // page1: './src/js/page-one.js'
  }, // 入口
  output: {
    /* #文件名称 */
    filename: assetsDir + `js/[name].${hashType}.js`,
    /* #输出目录 */
    path: path.resolve('./', outputPath)
    // globalObject: 'this'
  }, // 出口
  // 模块解析相关配置
  resolve: {
    /* #配置别名 */
    alias: {
      '@': path.resolve(__dirname, '../src/'),
      'art': path.resolve(__dirname, '../src/art'),
      'nm': path.resolve(__dirname, '../node_modules')
    },
    // https://segmentfault.com/q/1010000017072684
    // aliasFields: ['browser'],
    // 不开启强制文件扩展名
    enforceExtension: false,
    // 不开启强制模块扩展名
    enforceModuleExtension: false,
    // 自动解析的扩展名
    extensions: ['.js', '.json'],
    // 解析模块的入口字段顺序
    // mainFields: ['module', 'main'],
    mainFiles: ['index'],
    // 模块搜索目录
    modules: ['node_modules']
  },
  module: {
    // 配置不解析的库。去掉无依赖的库的解析，可以加快编译速度
    noParse: /jquery/,
    rules: [
      {
        test: /\.(png|jpg|jpeg|gif|bmp|svg)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              /* #【仅作展示】输出的文件名称 */
              name: (resourcePath, resourceQuery) => {
                console.log('图片地址打印', resourcePath)
                // return `[name].${hashType}.[ext]`
                return `[name].[ext]`
              },
              /* #小于8k的都转为Base64 */
              limit: 8192,
              // 关闭esModule以解决<img src="[object Module]"/>的问题
              esModule: false,
              /* #输出目录 */
              outputPath: assetsDir + 'img/'
            }
          }
          // {
          //   loader: 'image-webpack-loader',
          //   options: {
          //     mozjpeg: {
          //       progressive: true,
          //       quality: 65
          //     },
          //     // optipng.enabled: false will disable optipng
          //     optipng: {
          //       enabled: false,
          //     },
          //     pngquant: {
          //       quality: [0.65, 0.90],
          //       speed: 4
          //     },
          //     gifsicle: {
          //       interlaced: false,
          //     },
          //     // the webp option will enable WEBP
          //     webp: {
          //       quality: 75
          //     }
          //   }
          // }
        ]
      }, // 图片处理
      {
        test: /\.art$/i,
        loader: 'art-template-loader',
        options: {
          htmlResourceRules: ([
            /src="([^"]*)"/
          ])
        }
      },
      {
        test: /\.(htm|html)$/i,
        loader: 'html-loader',
        options: {
          attributes: {
            urlFilter: (attribute, value, resourcePath) => {
              console.log('文件匹配', value)
              return true
            }
          }
        }
      }, // 处理html中的图片
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              /* #输出的文件名称 */
              name: `[name].${hashType}.[ext]`,
              /* #指定公共路径，修正打包后的路径偏差 */
              publicPath: '../font',
              /* #输出目录 */
              outputPath: assetsDir + 'font'
            }
          }
        ]
      }, // 字体处理
      {
        enforce: 'pre',
        test: /\.js$/i,
        /* 忽略编译的js目录 */
        exclude: /node_modules/,
        include: /src/,
        use: 'happypack/loader?id=js'
        /* 不使用happypack */
        // use: [
        //   {
        //     loader: 'babel-loader'
        //   },
        //   {
        //     loader: 'eslint-loader'
        //   }
        // ]
      }, // js处理
      {
        test: /\.scss$/i,
        use: [
          /* #将css插入到<head></head>中 */
          // "style-loader",
          /* #将css单独提取一个文件 */
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              /* #纠正css文件中图片的路径 */
              publicPath: '../../',
              /* #开启css热加载 */
              hmr: true
            }
          },
          {
            loader: 'css-loader',
            options: {
              /* #启用css modules,启用后样式的命名会被编码 */
              // modules: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              // sassOptions: {
              //   /* #缩进
              //   indentWidth: 4,
              //   /* #添加文件目录@import的时候可以省去目录的书写 */
              //   /* #https://github.com/YutHelloWorld/Blog/issues/12 */
              //   includePaths: ['./src/style']
              // },
              /* #这里引入一个文件，可以实现对样式的复写 */
              // additionalData: (content, loaderContext) => {
              //   return content + '@import "./src/css/page-one";'
              // },
              /* #为打包的文件添加额外数据（如：全局变量）*/
              // additionalData: 'body{background-color: green;}'
            }
          },
          'postcss-loader' // 使用postcss处理css文件
        ]
      } // css 处理
    ]
  },
  plugins: [
    /* #设置环境变量 */
    new webpack.DefinePlugin({
      'process.env': {
        'TEST': JSON.stringify('for process test!!!')
      }
    }),
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      /* #title的名字 */
      title: 'Webpack4-Single-Page',
      /* #采用的模板 */
      template: './public/index.html',
      /* #网站图标 */
      favicon: './public/favicon.ico'
      /* #【仅作展示】输出的文件名 */
      // filename: 'index.html',
      /* #【仅作展示】引入的文件注入的位置 */
      // inject: 'body',
      /* #【仅作展示】不添加文件请求hash值(如：<script src="app.js?d101be3faed9895bcec3"></script>）*/
      // hash: false
    }),
    new MiniCssExtractPlugin({
      // filename: 'css/[name].[contenthash:8].css'
      /* #输出css文件目录及文件名 */
      filename: assetsDir + `css/[name].css`
      // filename: assetsDir + `css/[name].${hashType}.css`
    }),
    // 自动加载模块。不必每个文件都import 或 require
    // new webpack.ProvidePlugin({
    //   $: 'jquery'
    // }),
    new webpack.HotModuleReplacementPlugin({}),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new HappyPack({
      id: 'js',
      threads: 4, // 线程数
      verbose: true, // 是否允许 happypack 输出日志
      debug: false, // 是否允许 happypack 打印 log 分析信息
      loaders: ['babel-loader', 'eslint-loader']
    })
  ].concat(plugins)
  // externals: {
  //   'lodash/array': '_.array'
  // }
}
